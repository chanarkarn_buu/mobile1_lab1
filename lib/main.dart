import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Halo Flutter";
String thaiGreeting = "สวัสดี Flutter";
String chinaGreeting = "你好 Flutter";
String koreaGreeting = "안녕하세요 Flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        //Scaffold Widget
        home: Scaffold(
            appBar: AppBar(
              title: Text("Hello Flutter"),
              leading: Icon(Icons.home),
              actions: <Widget>[
                IconButton(onPressed:()  {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    spanishGreeting:englishGreeting;
                  });
                },
                    icon: Icon(Icons.refresh)),
                IconButton(onPressed:()  {
                  setState(() {
                    displayText = displayText == thaiGreeting?
                    chinaGreeting:thaiGreeting;
                  });
                },
                    icon: Icon(Icons.account_balance_sharp)),
                IconButton(onPressed:()  {
                  setState(() {
                    displayText = displayText == koreaGreeting?
                    chinaGreeting:koreaGreeting;
                  });
                },
                    icon: Icon(Icons.star_half_rounded ))

              ],
            ),
            body: Center(
              child: Text(
                displayText,
                style: TextStyle(fontSize: 24),
              ),
            )
        )
    );
  }
}
// class HelloFlutterApp extends StatelessWidget{
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//       appBar: AppBar(
//         title: Text("Hello Flutter"),
//         leading: Icon(Icons.home),
//         actions: <Widget>[
//           IconButton(onPressed: () {},
//               icon: Icon(Icons.refresh))
//         ],
//       ),
//         body: Center(
//           child: Text(
//             "Hello Flutter",
//             style: TextStyle(fontSize: 24),
//           ),
//         ),
//       ),
//     );
//   }
// }